utilisation du linux embarqué avec Chrome OS

- Activer le support de Linux

## Mon installation de départ

```bash
sudo apt-get install nano -y
sudo apt-get install htop -y

```

## Installer docker

- https://docs.docker.com/engine/install/debian/#install-using-the-repository


## Utiliser OpenVSCode Server

> ne semble pas fonctionner avec un chromebook

- J'ai construit ma propre image
- registry.gitlab.com/k33g_org/imgs/openvscode-server:latest

sudo docker run -it --init -p 3000:3000 -v "$(pwd):/home/workspace:cached" registry.gitlab.com/k33g_org/imgs/openvscode-server:latest
