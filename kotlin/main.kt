//import kotlin.String

class Human(var firstName: String, var lastName: String)

class Animal(val name: String)


fun main() {
  println("Hello 😛🎉")
  println("👋 Hello, World! 🌍")
  val bob = Human("Bob", "Morane")
  println("Hello ${bob.firstName} ${bob.lastName}")
  
  bob.firstName ="Bobby"
  bob.lastName = "Jackson"
  println("Hello ${bob.firstName} ${bob.lastName}")

  val wolf = Animal("Wolf")
  //wolf.name = "wolfy"

  println(wolf.name)

}
// unset JAVA_TOOL_OPTIONS
// kotlinc main.kt -include-runtime -d main.jar
// java -jar main.jar
// https://kotlinlang.org/docs/command-line.html#create-and-run-an-application