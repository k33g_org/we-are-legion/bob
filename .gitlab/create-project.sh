#!/bin/bash

# ./create-project.sh <group_id> <project_name>
# ./create-project.sh 13853882 hello-demo
# 🖐 use ./search-group.sh foo to get the id of the project

http POST https://gitlab.com/api/v4/projects "Private-Token":"${GITLAB_TOKEN_ADMIN}" "Content-Type":"application/json" namespace_id="$1" name="$2" initialize_with_readme=true visibility="public" --body --output project.json

jq '(.id|tostring) + " " + .web_url' --raw-output project.json